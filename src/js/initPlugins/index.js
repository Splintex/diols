import $ from 'jquery';
import perfectScrollbar from 'perfect-scrollbar/jquery';
import initFotorama from './fotorama';
import initFileUpload from './fileUpload';
import * as select from './select';

const initPlugins = () => {
  select.categorySelect('.js-select-category');
  select.locationSelect('.js-select-location');
  select.defaultSelect('.js-select');
  initFotorama();
  initFileUpload();
  $('.js-scrollbar').perfectScrollbar();
};

export default initPlugins;