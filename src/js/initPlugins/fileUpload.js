import $ from 'jquery';
import uploadPreview from '../libs/jquery.uploadPreview';

const initFileUpload = () => {
  $('.js-image-preview').each(function () {
    $.uploadPreview({
      input_field: $(this).find('input'),
      preview_box: $(this),
      label_field: $(this).find('label'),
      label_default: '',
      label_selected: '',
      success_callback: () => {}
    });
  });
  
};

export default initFileUpload;