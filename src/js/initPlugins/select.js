import select2 from 'select2';

const formatCategoryState = (state) => {
  if (!state.id) {
    return state.text;
  }
  const icon = state.element.value.toLowerCase();
  const $state = $(
    `<span class="select2__icon">
        <svg class="icon icon-${icon}">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-${icon}"></use>
        </svg>
    </span>
    <span class="select2__text">
        ${state.text}
    </span>`
  );
  return $state;
};

const formatLocationState = (state) => {
  if (!state.id) {
    return state.text;
  }
  const icon = state.element.value.toLowerCase();
  const $state = $(
    `<span>
        <span class="select2__icon">
          <i class="fa fa-map-marker"></i>
        </span>
        ${state.text}
    </span>`
  );
  return $state;
};

const categorySelect = (selector) => {
  $(selector).each(function () {
    $(this).find('select').select2({
      templateResult: formatCategoryState,
      templateSelection: formatCategoryState,
      minimumResultsForSearch: Infinity,
      dropdownParent: $(this)
    });
  });
};

const locationSelect = (selector) => {
  $(selector).each(function () {
    $(this).find('select').select2({
      templateSelection: formatLocationState,
      minimumResultsForSearch: Infinity,
      dropdownParent: $(this)
    });
  })
  
};

const defaultSelect = (selector) => {
  $(selector).each(function () {
    $(this).find('select').select2({
      minimumResultsForSearch: Infinity,
      dropdownParent: $(this)
    });
  });
};

export {categorySelect, locationSelect, defaultSelect};