import fotorama from '../libs/fotorama';

const initFotorama = () => {
  $('.js-fotorama').fotorama({
    width: 810,
    maxwidth: '100%',
    ratio: 16/9,
    allowfullscreen: true,
    nav: 'thumbs',
    thumbmargin: 10,
    allowfullscreen: true
  });
};

export default initFotorama;
