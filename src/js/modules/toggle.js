export default class Toggle {
  constructor(el) {
    this.btn = el;
    this.$btn = $(this.btn);
    this.$root = $('html');
    this.$doc = $(document);
    this._bindEvents();
  }
  
  _bindEvents() {
    this.$btn.on('click', this._clickAction.bind(this));
    this.$doc.on('click', this._hideElements.bind(this));
  }
  
  _clickAction(event) {
    const $active = $(event.currentTarget);
    const targetClass = $active.data('toggle');
    const rootClass = $active.data('class');
    const anim = $active.data('anim');
    const parent = $active.data('parent');
    const autoHide = +$active.data('autohide');
    const hide = $active.data('hide');
    const text = $active.data('text');
    event.preventDefault();
    if ($active.hasClass('is-active')) {
      if (parent) {
        if (autoHide) {
          this.hide(targetClass);
        }
        $active.parents(parent).find(targetClass).removeClass('is-active');
        $active.parents(parent).find('[data-toggle="' + targetClass + '"]').removeClass('is-active');
      }
      else {
        this.hide(targetClass);
      }
      if (text) {
        $active.data('text', $active.text()).text(text);
      }
    }
    else {
      if (parent) {
        if (autoHide) {
          this.hide(targetClass);
        }
        if (hide) {
          this.hide(hide);
        }
        $active.parents(parent).find(targetClass).addClass('is-active');
        $active.parents(parent).find('[data-toggle="' + targetClass + '"]').addClass('is-active');
      }
      else {
        if (hide) {
          this.hide(hide);
        }
        this.show(targetClass);
      }
      if (text) {
        $active.data('text', $active.text()).text(text);
      }
    }
    
    if (rootClass) {
      this.$root.toggleClass(rootClass);
    }
    if (anim == "slide") {
      $(targetClass).slideToggle(200);
    }
    if (anim == "fade") {
      $(targetClass).toggle(200);
    }
  }
  
  hide(el) {
    $(el).removeClass('is-active');
    $('[data-toggle="' + el + '"]').removeClass('is-active');
  }
  
  show(el) {
    $(el).addClass('is-active');
    $('[data-toggle="' + el + '"]').addClass('is-active');
  }
  
  _hideElements(event) {
    let $active = $(event.target);
    if (!$active.closest('.is-active').length) {
      let $toggle = $('[data-toggle].is-active');
      $toggle.removeClass('is-active');
      $($toggle.data('toggle')).removeClass('is-active');
    }
  }
  
}
