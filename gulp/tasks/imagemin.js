const gulp = require('gulp');
const imagemin = require('gulp-imagemin');

gulp.task('imagemin', () =>
  gulp.src('src/img/*.{png,jpg,jpeg,gif}')
    .pipe(imagemin())
    .pipe(gulp.dest('build/img'))
);