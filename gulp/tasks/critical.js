var gulp = require('gulp');
var gutil = require('gulp-util');
var critical = require('critical').stream;

gulp.task('critical', function () {
  return gulp.src('build/*.html')
    .pipe(critical({
      base: 'build/',
      inline: true,
      minify: true,
      css: ['build/css/app.css'],
      ignore: ['@font-face','@media'],
      extract: true,
      width: 1280,
      height: 800
    }))
    .on('error', function (err) {
      gutil.log(gutil.colors.red(err.message));
    })
    .pipe(gulp.dest('build'));
});